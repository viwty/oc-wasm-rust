//! Provides high-level access to the internet APIs.

use crate::error::Error;
use crate::helpers::{max_usize, Ignore};
use alloc::vec::Vec;
use minicbor::{Decode, Encode};
use oc_wasm_futures::invoke::{self, component_method, value_method, Buffer};
use oc_wasm_helpers::Lockable;
use oc_wasm_safe::computer;
use oc_wasm_safe::{
	component::{Invoker, MethodCallError},
	descriptor, extref, Address,
};

/// The type name for internet card components.
pub const TYPE: &str = "internet";

/// An internet component.
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Internet(Address);

impl Internet {
	/// Creates a wrapper around an internet card.
	///
	/// The `address` parameter is the address of the card. It is not checked for correctness
	/// at this time because network topology could change after this function returns; as such,
	/// each usage of the value may fail instead.
	#[must_use = "This function is only useful for its return value"]
	pub fn new(address: Address) -> Self {
		Self(address)
	}

	/// Returns the address of the internet card.
	#[must_use = "This function is only useful for its return value"]
	pub fn address(&self) -> &Address {
		&self.0
	}
}

impl<'invoker, 'buffer, B: 'buffer + Buffer> Lockable<'invoker, 'buffer, B> for Internet {
	type Locked = Locked<'invoker, 'buffer, B>;

	fn lock(&self, invoker: &'invoker mut Invoker, buffer: &'buffer mut B) -> Self::Locked {
		Locked {
			address: self.0,
			invoker,
			buffer,
		}
	}
}

/// An internet component on which methods can be invoked.
///
/// This type combines an internet card address, an [`Invoker`] that can be used to make method calls,
/// and a scratch buffer used to perform CBOR encoding and decoding. A value of this type can be
/// created by calling [`Internet::lock`], and it can be dropped to return the borrow of the
/// invoker and buffer to the caller so they can be reused for other purposes.
///
/// The `'invoker` lifetime is the lifetime of the invoker. The `'buffer` lifetime is the lifetime
/// of the buffer. The `B` type is the type of scratch buffer to use.
pub struct Locked<'invoker, 'buffer, B: Buffer> {
	/// The component address.
	address: Address,

	/// The invoker.
	invoker: &'invoker mut Invoker,

	/// The buffer.
	buffer: &'buffer mut B,
}

impl<'invoker, 'buffer, B: Buffer> Locked<'invoker, 'buffer, B> {
	pub async fn is_tcp_enabled(&mut self) -> Result<bool, Error> {
		let (enabled,) = component_method::<(), _, _>(
			self.invoker,
			self.buffer,
			&self.address,
			"isTcpEnabled",
			None,
		)
		.await?;

		Ok(enabled)
	}

	pub async fn is_http_enabled(&mut self) -> Result<bool, Error> {
		let (enabled,) = component_method::<(), _, _>(
			self.invoker,
			self.buffer,
			&self.address,
			"isHttpEnabled",
			None,
		)
		.await?;

		Ok(enabled)
	}

	pub async fn connect(&mut self, address: &str, port: Option<u16>) -> Result<(), Error> {
		todo!()
	}

	pub async fn request(
		&mut self,
		url: &str,
		data: Option<&str>,
		headers: (),
	) -> Result<HttpHandle, Error> {
		let descriptor: Result<(descriptor::Decoded,), MethodCallError<'_>> = component_method(
			self.invoker,
			self.buffer,
			&self.address,
			"request",
			Some(&(url, data)),
		)
		.await;
		let mut b = vec![];
		b.resize(512, 0);
		let descriptor = match descriptor {
			Ok((descriptor,)) => unsafe { descriptor.into_owned() },
			Err(MethodCallError::Other(e)) => computer::error(e.message(&mut b)?),
			Err(e) => computer::error(&e.to_string()),
		};

		Ok(HttpHandle { descriptor })
	}
}

/// A handle to a file open for reading.
///
/// The file is closed when the handle is dropped.
#[derive(Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct HttpHandle {
	/// The opaque value descriptor.
	descriptor: descriptor::Owned,
}

impl<'handle, 'invoker, 'buffer, B: 'buffer + Buffer> Lockable<'invoker, 'buffer, B>
	for &'handle HttpHandle
{
	type Locked = LockedHttpHandle<'handle, 'invoker, 'buffer, B>;

	fn lock(&self, invoker: &'invoker mut Invoker, buffer: &'buffer mut B) -> Self::Locked {
		LockedHttpHandle {
			handle: self,
			invoker,
			buffer,
		}
	}
}

/// A readable file handle on which methods can be invoked.
///
/// This type combines a readable file handle, an [`Invoker`] that can be used to make method
/// calls, and a scratch buffer used to perform CBOR encoding and decoding. A value of this type
/// can be created by calling [`ReadHandle::lock`](Lockable::lock), and it can be dropped to return
/// the borrow of the invoker and buffer to the caller so they can be reused for other purposes.
///
/// The `'handle` lifetime is the lifetime of the original file handle. The `'invoker` lifetime is
/// the lifetime of the invoker. The `'buffer` lifetime is the lifetime of the buffer. The `B` type
/// is the type of scratch buffer to use.
pub struct LockedHttpHandle<'handle, 'invoker, 'buffer, B: Buffer> {
	/// The file handle.
	handle: &'handle HttpHandle,

	/// The invoker.
	invoker: &'invoker mut Invoker,

	/// The buffer.
	buffer: &'buffer mut B,
}

impl<'handle, 'invoker, 'buffer, B: Buffer> LockedHttpHandle<'handle, 'invoker, 'buffer, B> {
	pub async fn read(self, length: Option<usize>) -> Result<Option<&'buffer [u8]>, Error> {
		use minicbor::bytes::ByteSlice;
		let ret: Result<(Option<&'buffer ByteSlice>,), MethodCallError<'_>> = value_method(
			self.invoker,
			self.buffer,
			&self.handle.descriptor,
			"read",
			length.map(|n| (n,)).as_ref(),
		)
		.await;
		match ret {
			Ok((Some(bytes),)) => Ok(Some(bytes)),
			Ok((None,)) => Ok(None),
			Err(MethodCallError::Other(exception)) => {
				if exception.is_type("java.io.IOException") {
					// The borrow checker isn’t quite smart enough to notice that we can drop the
					// borrow of “buffer” and reuse it in this branch because we’re not going to
					// return anything that depends on it. Fortunately, the error string we’re
					// looking for is small and of fixed size, so just use a fixed-size buffer
					// instead.
					const NOT_ENOUGH_ENERGY: &str = "not enough energy";
					let mut message_buffer = [0_u8; NOT_ENOUGH_ENERGY.len()];
					match exception.message(&mut message_buffer) {
						Ok(m) if m == NOT_ENOUGH_ENERGY => Err(Error::NotEnoughEnergy),
						_ => Err(Error::BadComponent(oc_wasm_safe::error::Error::Unknown)),
					}
				} else {
					Err(Error::BadComponent(oc_wasm_safe::error::Error::Unknown))
				}
			}
			Err(e) => Err(e.into()),
		}
	}

	pub async fn finish_connect(&mut self) -> Result<bool, Error> {
		let (res,) = value_method::<(), _, _, _>(
			self.invoker,
			self.buffer,
			&self.handle.descriptor,
			"finishConnect",
			None,
		)
		.await?;

		Ok(res)
	}
}
